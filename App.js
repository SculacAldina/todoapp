import React from 'react';
import {View, StyleSheet} from 'react-native';
import TodoState from './src/context/todo/TodoState';

import Header from './src/components/Header';
import TaskList from './src/components/TaskList';

const App = () => {
  return (
    <TodoState>
      <Header title="My Todo List" />
      <View style={styles.container}>
        <TaskList />
      </View>
    </TodoState>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDF0FE',
  },
});

export default App;
