import {ADD_TODO_LIST, EDIT_TODO_LIST, DELETE_TODO_LIST} from '../types';

export default (state, action) => {
  switch (action.type) {
    case ADD_TODO_LIST:
      return {
        ...state,
        tasks: [action.payload, ...state.tasks],
      };
    case EDIT_TODO_LIST:
      return {
        ...state,
        tasks: [
          ...state.tasks.map(item =>
            item.id === action.payload ? {...item, done: true} : item,
          ),
        ],
      };
    case DELETE_TODO_LIST:
      return {
        ...state,
        tasks: state.tasks.filter(item => item.id !== action.payload),
      };
    default:
      return state;
  }
};
