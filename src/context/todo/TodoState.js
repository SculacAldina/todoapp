import React, {useReducer} from 'react';
import TodoContext from './todoContext';
import TodoReducer from './todoReducer';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import {ADD_TODO_LIST, EDIT_TODO_LIST, DELETE_TODO_LIST} from '../types';

const todoList = [
  {id: uuidv4(), text: 'Milk', done: false},
  {id: uuidv4(), text: 'Apples', done: true},
  {id: uuidv4(), text: 'Pizza', done: true},
  {id: uuidv4(), text: 'Eggs', done: false},
  {id: uuidv4(), text: 'Juice', done: false},
];

const TodoState = props => {
  const initialState = {
    tasks: todoList,
  };

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  const addTask = task => {
    if (task !== '') {
      dispatch({
        type: ADD_TODO_LIST,
        payload: {id: uuidv4(), text: task, done: false},
      });
    }
  };

  const updateTask = id => {
    // console.log(id);
    dispatch({
      type: EDIT_TODO_LIST,
      payload: id,
    });
  };

  const deleteTask = id => {
    dispatch({
      type: DELETE_TODO_LIST,
      payload: id,
    });
  };

  return (
    <TodoContext.Provider
      value={{
        tasks: state.tasks,
        addTask,
        deleteTask,
        updateTask,
      }}>
      {props.children}
    </TodoContext.Provider>
  );
};

export default TodoState;
