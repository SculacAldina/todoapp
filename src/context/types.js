export const GET_TODO_LIST = 'GET_TODO_LIST';
export const ADD_TODO_LIST = 'ADD_TODO_LIST';
export const EDIT_TODO_LIST = 'EDIT_TODO_LIST';
export const DELETE_TODO_LIST = 'DELETE_TODO_LIST';
