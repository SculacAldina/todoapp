import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import InputForm from './InputForm';
import LinearGradient from 'react-native-linear-gradient';
import Logo from '../img/unicorn.png';

const Header = ({title}) => {
  const logoUri = Image.resolveAssetSource(Logo).uri;

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      colors={['#667cff', '#8a66ff']}
      style={styles.header}>
      <Image source={{uri: logoUri}} style={styles.logo} />
      <InputForm />
    </LinearGradient>
  );
};

Header.defaultProps = {
  title: 'My Todo List',
};

const styles = StyleSheet.create({
  header: {
    height: 300,
    elevation: 15,
    // background color must be set
    backgroundColor: '#000', // invisible color
  },
  logo: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    marginTop: 55,
  },
});

export default Header;
