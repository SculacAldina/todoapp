import React, {useState, useContext} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import todoContext from '../context/todo/todoContext';
import Icon from 'react-native-vector-icons/FontAwesome';

const InputForm = () => {
  const {addTask} = useContext(todoContext);
  const [text, setText] = useState('');

  const onChange = textValue => {
    setText(textValue);
  };

  const onSubmit = () => {
    addTask(text);
    setText('');
  };

  return (
    <View style={styles.root}>
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          selectionColor="#8a66ff"
          placeholderTextColor="#8a66ff"
          placeholder="Enter new task..."
          onChangeText={onChange}
          onSubmitEditing={() => onSubmit()}
          value={text}
        />
        <Icon
          style={styles.icon}
          name="plus"
          size={30}
          color={text === '' ? '#ccc' : '#8a66ff'}
          onPress={() => onSubmit()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#fff',
    height: 56,
    width: 275,
    alignSelf: 'center',
    borderRadius: 17,
    marginTop: 45,
    justifyContent: 'center',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 10,
  },
  input: {
    width: '85%',
    //backgroundColor: 'red',
  },
  icon: {
    marginLeft: 10,
  },
});

export default InputForm;
