import React, {useContext} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import todoContext from '../context/todo/todoContext';
import Icon from 'react-native-vector-icons/FontAwesome';

const TaskItem = ({task, done, taskId}) => {
  const {deleteTask, updateTask} = useContext(todoContext);

  return (
    <TouchableOpacity style={styles.root}>
      <View style={styles.container}>
        <Text style={done ? styles.textDone : styles.text}>{task}</Text>
        <View style={styles.containerIcons}>
          {!done ? (
            <Icon
              style={styles.icon}
              name="check"
              size={20}
              color="#72CDA2"
              onPress={() => updateTask(taskId)}
            />
          ) : null}
          <Icon
            style={styles.icon}
            name="trash-o"
            size={20}
            color="#E88C98"
            onPress={() => deleteTask(taskId)}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#fff',
    minHeight: 58,
    width: 304,
    borderRadius: 15,
    marginTop: 20,
    marginBottom: 10,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  containerIcons: {
    flexDirection: 'row',
  },
  icon: {
    marginLeft: 30,
  },
  text: {
    fontWeight: '600',
    fontSize: 16,
    color: '#000',
    maxWidth: '60%',
    paddingVertical: 5,
  },
  textDone: {
    fontSize: 16,
    color: '#ccc',
    fontStyle: 'italic',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    maxWidth: '60%',
    paddingVertical: 5,
  },
});

export default TaskItem;
