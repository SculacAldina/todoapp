import React, {useContext} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import todoContext from '../context/todo/todoContext';
import TaskItem from './TaskItem';

const TaskList = () => {
  const {tasks} = useContext(todoContext);
  return (
    <View style={styles.container}>
      <FlatList
        data={tasks}
        renderItem={({item}) => (
          <TaskItem task={item.text} done={item.done} taskId={item.id} />
        )}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //marginTop: 20,
  },
});

export default TaskList;
